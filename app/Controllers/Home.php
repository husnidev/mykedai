<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$db = db_connect();
		$query = $db->query('SELECT * FROM gambar');

		$all_pekan = $query->getResult(); 
		
		return view('homepage', ['all_pekan' => $all_pekan]); 
	}
}
