<?php

namespace App\Controllers;

class Gambar extends BaseController
{

	function __construct() {
		$this->session = \Config\Services::session();
	}
	
	public function index()
	{
		$gambar_model = new \App\Models\GambarModel();
		$data = [
			'gambar' => $gambar_model->orderBy('id','desc')->paginate(3),
			'pager' => $gambar_model->pager,
		];

        return view('admin/listing', $data);
    }

    function add(){
        helper('form');
        return view('admin/add');

    }

    function save_new() {
		 
		$gambar_model = new \App\Models\GambarModel();

		$data = [
			'name' => $this->request->getPost('name'),
			'description' => $this->request->getPost('description')
		];

		$file = $this->request->getFile('photo_path');

		//dd($files);

		// Grab the file by name given in HTML form
		if ($file)
		{		
			// Generate a new secure name
			$photo_path = $file->getRandomName();
		
			// Move the file to it's new home
			$file->move('img/', $photo_path);

			$data['photo_path'] = $photo_path;
		
		}

		$gambar_model->insert( $data );

		$_SESSION['success'] = true;
		$this->session ->markAsFlashdata('success');

		return redirect()->to('/gambar');
		// echo "<h1>HELOO ... saya akan save data</h1>";
	}

	function edit($myid) {
		helper('form');
		$gambar_model = new \App\Models\GambarModel();

		$gambar = $gambar_model->find($myid);

		return view ('admin/edit', [ 'gambar' => $gambar ]);
		//echo '<h1>Edit Photo Here</h1>';
	}

	function save_edit($myid) { 

		$gambar_model = new \App\Models\GambarModel();

		$data = [
			'name' => $this->request->getPost('name'),
			'description' => $this->request->getPost('description'),
		];

		$file = $this->request->getFile('photo_path');

		//dd($files);

		// Grab the file by name given in HTML form
		if ($file)
		{		
			// Generate a new secure name
			$photo_path = $file->getRandomName();
		
			// Move the file to it's new home
			$file->move('img/', $photo_path);

			$data['photo_path'] = $photo_path;
		
		}
		$gambar_model->update($myid,$data);

		$_SESSION['success'] = true;
		$this->session ->markAsFlashdata('success');

		return redirect()->to('/gambar/edit/'. $myid);
	}

	function delete($myid){

	$gambar_model = new \App\Models\GambarModel();

	$gambar_model->where('id',$myid)->delete();

	$_SESSION['deleted'] = true;
	$this->session ->markAsFlashdata('deleted');
	
	return redirect()->back();
	}
}
